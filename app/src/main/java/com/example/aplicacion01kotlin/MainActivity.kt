package com.example.aplicacion01kotlin

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast

class MainActivity : AppCompatActivity() {
    private lateinit var btnSaludar: Button
    private lateinit var btnLimpiar: Button
    private lateinit var btnSalir: Button
    private lateinit var lblSaludar: TextView
    private lateinit var txtNombre: EditText
    private lateinit var imageHola: ImageView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        btnSaludar = findViewById(R.id.btnSaludar) as Button
        btnLimpiar  = findViewById(R.id.btnLimpiar) as Button
        btnSalir  = findViewById(R.id.btnSalir) as Button
        lblSaludar   =  findViewById(R.id.lblSaludar) as TextView
        txtNombre =  findViewById(R.id.txtNombre) as EditText
        imageHola = findViewById(R.id.imageHola) as ImageView

        btnSaludar.setOnClickListener {
            if (txtNombre.text.toString().contentEquals("")){
                Toast.makeText(applicationContext,"Captura el dato correspondiente",Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            val saludar= "Hola" + txtNombre.text.toString()+" " + "¿Como estas?"
            lblSaludar.text = saludar
        }

        btnLimpiar.setOnClickListener {
            lblSaludar.text = ""
            txtNombre.setText("")
            txtNombre.requestFocus()
        }

        btnSalir.setOnClickListener {
            finish()
        }


    }
}